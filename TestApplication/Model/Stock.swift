//
//  Stock.swift
//  TestApplication
//
//  Created by Asan Ametov on 11.10.2020.
//

import UIKit

class Stock {
    let date: Date
    let cost: Int
    var buy: Bool? = nil
    var sale: Int? = nil

    init(date: Date, cost: Int) {
        self.date = date
        self.cost = cost
    }
}
