//
//  TableViewController.swift
//  TestApplication
//
//  Created by Asan Ametov on 11.10.2020.
//

import UIKit

class StocksTableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var sumLabel: UILabel!
    var viewModel: ChartViewModel? = nil

    init(viewModel: ChartViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "StocksTableViewController", bundle: Bundle.main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Localization.string("open_details")
        self.addRightBarButton()
        self.setupTableView()
        self.updateData()
    }

    func setupTableView() {
        self.tableView.registerCellNib(StockTableViewCell.self)
        self.footerView.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 60)
        self.tableView.tableFooterView = self.footerView
    }
    
    func updateData() {
        guard let viewModel = self.viewModel else {
            return
        }
        let sum = viewModel.checkSum()
        let format = Localization.string("sum_format")
        self.sumLabel.text =  String(format: format, sum)
        self.tableView.reloadData()
    }
    
    func addRightBarButton() {
        let button: UIButton = UIButton(type: .custom) 
        button.setImage(UIImage(named: "closeImage"), for: .normal)
        button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func closeAction() {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension StocksTableViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
     
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.stocks.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(StockTableViewCell.self)) as! StockTableViewCell

        if let stock = self.viewModel?.stocks[indexPath.row] {
            cell.setupWith(stock: stock)
        }
        return cell
    }
    
}
