//
//  StockTableViewCell.swift
//  TestApplication
//
//  Created by Asan Ametov on 11.10.2020.
//

import UIKit

class StockTableViewCell: UITableViewCell {
    
    @IBOutlet weak var benefitLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(stock: Stock) {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMMM YYYY"
        self.costLabel.text = String(stock.cost)
        self.dateLabel.text = dateFormatterPrint.string(from: stock.date)
        if let buy = stock.buy, buy {
            self.benefitLabel.text = Localization.string("buy")
            self.benefitLabel.textColor =  UIColor.customRed
        }  else if let buy = stock.sale, (buy != 0) {
            self.benefitLabel.text =  "+\(buy) " + Localization.string("sale")
            self.benefitLabel.textColor =  UIColor.customGreen
        } else {
            self.benefitLabel.text = ""
        }
    }
}
