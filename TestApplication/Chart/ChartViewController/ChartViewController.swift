//
//  ChartViewController.swift
//  TestApplication
//
//  Created by Asan Ametov on 11.10.2020.
//

import UIKit
import Charts

class ChartViewController: UIViewController, ChartViewDelegate {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var chartView: BarChartView!
    var dataEntry : [BarChartDataEntry] = []
    let viewModel = ChartViewModel()
    @IBOutlet weak var saleTitleLabel: UILabel!
    @IBOutlet weak var buyTitleLabel: UILabel!
    @IBOutlet weak var openDetailsButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        chartView.delegate = self
        self.barChatSetup()
        self.localized()
        // Do any additional setup after loading the view.
    }

    func localized() {
        self.buyTitleLabel.text = Localization.string("buy")
        self.saleTitleLabel.text = Localization.string("sale")

    }
    func barChatSetup() {
        self.viewModel.getStocksFromXLSX()
        let sum = self.viewModel.checkSum()
        let format = Localization.string("sum_format")
        self.descriptionLabel.text =  String(format: format, sum)
        self.openDetailsButton.setTitle(Localization.string("open_details"), for: .normal)
        self.chartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInOutCubic)

        self.chartView.backgroundColor = UIColor.white
        for (index, item) in self.viewModel.stocks.enumerated() {
            let entry = BarChartDataEntry(x: Double(index), y: Double(item.cost))
            dataEntry.append(entry)
        }
        let chartDataSet = BarChartDataSet(entries: dataEntry, label: "2020")
        chartDataSet.colors = self.viewModel.getColors()
        let chartData = BarChartData(dataSet: chartDataSet)
        chartData.setDrawValues(false)
        let xAxis = self.chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.axisMinimum = 0
        xAxis.granularity = 0
        xAxis.granularityEnabled = true
        xAxis.labelCount = self.viewModel.stocks.count
        xAxis.valueFormatter = self
        xAxis.labelRotationAngle = CGFloat(-90)
        chartView.data = chartData
    }
    
    @IBAction func openDetailsAction(_ sender: UIButton) {
        let detailsVC = StocksTableViewController(viewModel: self.viewModel)
        detailsVC.modalPresentationStyle = .formSheet
        let navVC = UINavigationController(rootViewController: detailsVC)

        self.present(navVC, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChartViewController: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let index = Int(value)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM"
        return dateFormatterPrint.string(from: self.viewModel.stocks[index].date)
    }
}
