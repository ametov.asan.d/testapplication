//
//  ChartViewModel.swift
//  TestApplication
//
//  Created by Asan Ametov on 11.10.2020.
//

import Foundation
import CoreXLSX
class ChartViewModel {
    
    var stocks:[Stock] = []
    
    func getStocksFromXLSX() {
        var stockArray: [Stock] = []
        guard let path = Bundle.main.path(
          forResource: "task_ios",
          ofType: "xlsx"
        ), let file = XLSXFile(filepath: path) else {
            return
        }
        do {
            let rowA = try file.parseWorksheetPaths()
              .compactMap { try file.parseWorksheet(at: $0) }
              .flatMap { $0.data?.rows ?? [] }
                .flatMap { $0.cells }
                .filter ({($0.reference.column.value == "A")})
                .map({ (cell) -> Date in
                    if let date = cell.dateValue {
                                return date
                            } else {
                                return Date()
                            }
                })
            let rowB = try file.parseWorksheetPaths()
              .compactMap { try file.parseWorksheet(at: $0) }
              .flatMap { $0.data?.rows ?? [] }
              .flatMap { $0.cells }
              .filter({($0.reference.column.value == "B")})
              .map({$0.value})
            
            var i = 1
            while i < rowB.count {
                let date = rowA[i]
                let cost = Int(rowB[i] ?? "0")
                let stock = Stock(date: date, cost: cost ?? 0)
                stockArray.append(stock)
                i += 1
            }
            self.stocks = stockArray
        } catch {
            self.stocks = []
        }
    }
    func checkSum()-> String {
        let stockArrayComponents = self.getComponentsArray(array: self.stocks)
        var sum = 0
        stockArrayComponents.forEach { (components) in
            if let min = components.min(by: { $0.cost < $1.cost }), let max = components.max(by: { $0.cost < $1.cost }), components.count > 1 {
                let benefit = max.cost - min.cost
                max.sale = benefit
                
                min.buy = true
                sum += benefit
            }
        }
        return String(sum)
    }
    func getColors() -> [UIColor] {

        var colors:[UIColor] = []
        self.stocks.forEach { (stock) in
            if let buy = stock.buy, buy {
                colors.append(UIColor.customRed)
            } else if let sale = stock.sale, sale > 0 {
                colors.append(UIColor.customGreen)
            } else {
                colors.append(UIColor.customGray)
            }
        }
        return colors
    }
    func getComponentsArray(array:[Stock]) -> [[Stock]]{
        var i = 0
        let tmpArray:[Stock] = array
        var componentsArray:[[Stock]] = [[]]
        var lastIndex = 0;
        while i < array.count {
            let currentValue = array[i]
            if (i + 1 < array.count) {
                let nextValue = array[i+1]
                if (nextValue.cost < currentValue.cost) {
                    let range = lastIndex...i
                    let tmp:[Stock] = Array(tmpArray[range])
                    
                    componentsArray.append(tmp)
                    lastIndex = i + 1
                }
            } else if i < array.count {
                let range = lastIndex...i
                let tmp:[Stock] = Array(tmpArray[range])

                componentsArray.append(tmp)
            }
            i += 1
        }
        return componentsArray
    }
}
