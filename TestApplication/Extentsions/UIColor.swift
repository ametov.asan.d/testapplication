//
//  UIColor.swift
//  TestApplication
//
//  Created by Asan Ametov on 11.10.2020.
//

import UIKit

extension UIColor {
    static let customRed = UIColor.systemRed.withAlphaComponent(0.5)
    static let customGreen = UIColor.systemGreen.withAlphaComponent(0.5)
    static let customGray = UIColor.gray.withAlphaComponent(0.5)
}
