//
//  String.swift
//  TestApplication
//
//  Created by Asan Ametov on 11.10.2020.
//

import Foundation

extension String {
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
}
