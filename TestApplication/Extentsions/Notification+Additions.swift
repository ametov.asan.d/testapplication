//
//  Notification+Additions.swift
//  TestApplication
//
//  Created by Asan Ametov on 11.10.2020.
//

import UIKit

extension Notification.Name {
    static let DidChangeLanguage = Notification.Name(rawValue: "DidChangeLanguage")
}
