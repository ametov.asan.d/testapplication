//
//  AppDelegate.swift
//  TestApplication
//
//  Created by Asan Ametov on 11.10.2020.
//

import UIKit
import CoreXLSX
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window?.rootViewController = ChartViewController()
        self.window?.makeKeyAndVisible()

        return true
    }

}


